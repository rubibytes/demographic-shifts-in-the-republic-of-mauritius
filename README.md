# Demographic Shifts in the Republic of Mauritius

### Analyzing Trends in Fertility, Marriage, and Age Dynamics – Implications for the Future
Date of report: 23 December 2023

## Introduction
In September 2022, my wife and I relocated from South Africa to Mauritius. The transition was smoother than we anticipated, thanks in part to the welcoming nature of the community and the support from the government in making the move for expats a relatively simple process.

As expats, I noticed how many around us were also not native to Mauritius.

Conversations with local residents, however, unveiled a trend among the youth: many expressed aspirations to study and establish careers abroad, particularly in countries like France and the UK.

This trend sparked my curiosity about the demographic patterns in Mauritius. I became interested in exploring how the population was evolving, especially considering the factors of emigration and immigration. This report explores the data to uncover the underlying population dynamics in Mauritius, offering insights into its future.

## Why This Matters?
Mauritius is a unique nation, marked by a varied mix of cultures in addition to its own distinctive Mauritian culture. While still a relatively new post-colonial country (having gained independence in 1968), it presents a unique blend of traditional values and modern aspirations, and should be protected. But in the face of shifting demographic trends, the future may hold significant changes.

According to the Organisation for Economic Co-operation and Development (OECD), “a total fertility rate of 2.1 children per woman ensures a broadly stable population” (OECD, 2023).

Declining fertility rates can affect not only the natural increase in population growth, but also affects age demographics – leading to an aging population. An aging population brings its own challenges, such as a shrinking workforce, and an increase on social welfare systems. There is also the possibility of a rising cost of living.

If the natural increase in population declines, the net international migration can have a bigger impact on the population overall.

A decrease in fertility rates can be impacted by a number of factors, including (among others) getting married later, fewer people getting married, couples choosing to not have children, and divorce.

Many of these variables are interrelated, the age of a bride being married and fertility rates are related.

## Methodology
### Data Source and Collection
The source of all the data comes from the office of Statistics Mauritius.

Statistics Mauritius provides detailed statistics for the Republic of Mauritius, which includes the Island of Mauritius, the island of Rodrigues, and Agalega and St Brandon.

“Statistics Mauritius is responsible for the collection, compilation, analysis and dissemination of official statistics relating to all aspects of the economic, demographic and social activities with a few exceptions like fisheries and health statistics which fall under the responsibility of the respective Ministry, and banking and Balance of Payment statistics for which the Bank of Mauritius is responsible.

Statistics Mauritius falls under the aegis of the Ministry of Finance, Economic Planning and Development. It has decentralized its activities through the creation of Statistical Units in many Government Ministries. These units are staffed by officers on the establishment of Statistics Mauritius but directly servicing their respective Ministries in all statistical matters”

– https://statsmauritius.govmu.org
They are a decentralized entity of the government and operate on the .govmu domain.

I downloaded each “Digest of Demographic Statistics” from 1991 to 2000, and 2005 to 2022 found [here](https://statsmauritius.govmu.org/Pages/Statistics/By_Subject/Population/SB_Population.aspx) and [here](https://statsmauritius.govmu.org/Pages/Statistics/By_Subject/Population/Arch_Population.aspx).

The years 2001 – 2004 are not available on the Statistics Mauritius website (though they note they can be obtained in hard copy).

While the digests go all the way back to 1985, I decided to only download as far as 1991. (These digests would include data from previous years, for example the Live Birth obtained all the way to 1977).

I used the data for the Republic of Mauritius.

### Data Cleaning and Preparation
The data often has retroactive corrections, so data published in 1998 for example may be corrected in the 1999 publication, for example. For this reason I collated all the data by working from the most current year 2022 and working backwards so that the spreadsheet contains the corrected data.

The Digests were in PDF format only up until and including 2013. From 2014 to 2018, the Digests were available in Excel and PDF formats. And from 2019 onward the Digests were available in Excel format only.

Where Excel versions of the data were available, I used these.

I created my own workbook, and manually copied over, or typed where necessary, the data into my spreadsheets.

The newer PDF’s were searchable, but some of the older PDF’s were scans of some of which was difficult to decipher. In these cases I used a Python script using OCR to help determine the actual data. This was quite rare, as most of it was easy to work out by comparing adjacent cells or totals and working out what the value was.

### Data Analysis Methodology
Once I had all the relevant data in my Excel workbook, I then calculated additional data points that would be useful in the analysis. For example, calculating the Marriage rate per 1000 population, which offers a more detailed perspective on the relationship between marriage frequencies and population size.

I calculated key statistical measures including the mean, median, standard deviation, sample size, standard error, margin of error, and confidence intervals (both lower and upper limits). These measures provided a comprehensive understanding of the data’s distribution and reliability.

For a clearer interpretation of the trends and patterns, I created a variety of visual representations:

- Line, bar, and area charts to illustrate trends and changes over time. A heatmap to provide a visual summary of the aging of brides over time.
I then moved on to Power BI. I first reformatted the raw data into the correct format.

In Power BI:

- I used a combination offline, bar, and area charts to show the changes over time of the respective data points.
- I employed forecasting techniques to predict future trends based on historical data.
- I applied filters to drill down into specifics and isolate key factors.
Finally, I used Jupyter Notebook to create an advanced heatmap using Seaborn, providing deeper insights into our dataset.

Each step in our methodology was chosen to maximize the clarity and comprehensiveness of our analysis. By combining traditional statistical methods with advanced data visualization tools, I aimed to provide a holistic understanding of the data trends and their broader implications.

## Key Findings
Below are the key findings from this analysis:

- The overall population is aging, with a lot more weight shifting to 50-69 yrs. The ideal population pyramid would have a broad base, which indicates a large proportion of young people, and then gradually narrowing towards the top, indicating fewer elderly individuals. This can be seen if you select ‘1991’ in the “Comparison of Population Make Up by Age” graph.
- The Live Birth Rate has decreased drastically, even when normalised per 1000 population, from 20.59 in 1991 to 9.59 in 2022.
- In 2021, the Natural Increase (calculated by subtracting Deaths from Live Births) of the population dipped into the negative for the first time. This means that since 2021, more people been dying, than have been born.
- The rate of marriage is declining and at the same time, the rate of divorce is increasing.
- While the rate of marriage is declining, the average age of a bride is getting older over time. Up until 2000, the majority were in the 20-24yr age group, with the second highest being the 15-19 age group. From 2005 this has gradually shifted to the ,ajority of brides being in the 25-29yr age group.
- The total fertility rate dropped from 2.3 in 1991, to an incredibly low rate of 1.32 in 2022.
- Net international migration has gradually been increasing since 1990, with some years in the 1990’s and 2000’s showing a net positive.
There are all interrelated factors. For example, there is a decrease in the fertility rate every single decade, and at the same time there are fewer people getting married and among the married, are getting married later, as well as more couples are then getting divorced.

This is against the global phenomenon of men having markedly [lower testosterone levels than previous generations](https://www.healio.com/news/endocrinology/20120325/generational-decline-in-testosterone-levels-observed), in additional to decreases in testosterone [due to obesity](https://www.buffalo.edu/news/releases/2012/10/13739.html), which is increasingly [becoming a problem in Mauritius](https://health.govmu.org/health/?p=6623). Decreased testosterone also impacts total fertility rates within a population.

### Link to the report in Microsoft Power BI:
https://app.powerbi.com/view?r=eyJrIjoiNWQ2ZTczYTAtNGJjMC00NDQ1LTk0MzgtM2FmNzJkODk4NmVhIiwidCI6IjJiYjZjNGJiLWVlODEtNDI4YS05ZGEwLTg1YTA2Mjg5MDUzNSJ9&pageName=ReportSection

### Limitations
There is data from four Digests that were not included – in particular from 2001 – 2004. The years 2001 – 2004 are not available on the Statistics Mauritius website (though they note they can be obtained in hard copy). There is no reason given as to why only these are not available in soft copy.

Because these four years were missing I was unable to compare trends across a full 30 years for some of the graphs and analysis.

## Conclusion and Recommendations
Mauritius is not and has not been at replacement level since 1996.

All around the word, countries are desperately trying to fix this with many different measures to tackle this problem. It is a very serious problem there are entire fields of study that look at these problems and the impact of population pyramids being top heavy.

As many countries around the world are doing, the government could look at creating initiatives and drives to address that is the rapid decline of their respective populations.

[Italy offers a ‘baby bonus’](https://www.reuters.com/sustainability/society-equity/italys-prysmian-pay-baby-bonus-boosts-parental-leave-2023-08-07/) for each newborn child, while Japan introduced policies in the 1990s already to encourage couples.

Government could look at also working to improve the overall health of the population, and reducing obesity.

Is is not just about what the data is showing in the present, but where will it be in 20, 30 years from now. Another way to put this is, what future will your children or their children have?

Appendices and References
Statistics Mauritius: https://statsmauritius.govmu.org/

https://statsmauritius.govmu.org/Pages/Statistics/By_Subject/Population/SB_Population.aspx

https://statsmauritius.govmu.org/Pages/Statistics/By_Subject/Population/Arch_Population.aspx

OECD (2023), Fertility rates (indicator). doi: 10.1787/8272fb01-en (Accessed on 23 December 2023)

https://www.healio.com/news/endocrinology/20120325/generational-decline-in-testosterone-levels-observed

https://www.buffalo.edu/news/releases/2012/10/13739.html

https://health.govmu.org/health/?p=6623

https://www.reuters.com/sustainability/society-equity/italys-prysmian-pay-baby-bonus-boosts-parental-leave-2023-08-07/
