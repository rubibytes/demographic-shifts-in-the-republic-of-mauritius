from PIL import Image
import pytesseract

image = Image.open('/path/to/image.png')

text = pytesseract.image_to_string(image)

print(text)
